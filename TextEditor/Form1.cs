﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;

namespace TextEditor
{
    public partial class frmTextEditor : Form
    {
        private Size formOriginalSize;
        public frmTextEditor()
        {
            InitializeComponent();
        }
        private void FrmTextEditor_Load_1(object sender, EventArgs e)
        {
            formOriginalSize = this.Size;
        }
        private void NewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Deseja salvar as alterações antes criar um novo documento?", "Salvar", MessageBoxButtons.YesNoCancel);
            if (dr == DialogResult.Yes)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Text Files (.txt)|*.txt";
                saveFileDialog.Title = "Save file...";
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(saveFileDialog.FileName);
                    streamWriter.Write(rtbText.Text);
                    streamWriter.Close();
                }
            }
            else if (dr == DialogResult.No)
            {
                rtbText.Clear();
            }
        }    
        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("Deseja salvar as alterações antes de sair?", "Salvar", MessageBoxButtons.YesNoCancel);
            if (dr == DialogResult.Yes)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Text Files (.txt)|*.txt";
                saveFileDialog.Title = "Save file...";
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(saveFileDialog.FileName);
                    streamWriter.Write(rtbText.Text);
                    streamWriter.Close();
                }
            }
            else if (dr == DialogResult.No)
            {
                Application.Exit();
            }
        }
        private void OpenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog newFile = new OpenFileDialog();
            newFile.Filter = "Text Files (.txt)|*.txt";
            newFile.Title = "Open a file...";
            if (newFile.ShowDialog() == DialogResult.OK)
            {
                System.IO.StreamReader streamReader = new System.IO.StreamReader(newFile.FileName);
                rtbText.Text = streamReader.ReadToEnd();
                streamReader.Close();
            }
        }
        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text Files (.txt)|*.txt";
            saveFileDialog.Title = "Save file...";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(saveFileDialog.FileName);
                streamWriter.Write(rtbText.Text);
                streamWriter.Close();
            }

        }
        private void RedoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbText.Redo();
        }
        private void UndoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbText.Undo();
        }
        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbText.Cut();
        }
        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbText.Copy();
        }
        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbText.Paste();
        }
        private void SelectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbText.SelectAll();
        }
        private void LightModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbText.BackColor = Color.White;
            rtbText.ForeColor = Color.Black;
        }

        private void DarkModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbText.BackColor = Color.Black;
            rtbText.ForeColor = Color.LimeGreen;
        }
        private void MrGlassModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbText.BackColor = Color.WhiteSmoke;
            rtbText.ForeColor = Color.Purple;
        }
    }
}
